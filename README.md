# Buckley's & None House of Representatives Forecast
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/martintburgess/buckleys_public/master?filepath=notebooks%2FRun2016Forecast.ipynb)

A small repository that demonstrates the Buckley's & None approach to forecasting elections. In the Run2016Forecast notebook we demonstrate how the model would have performed at the 2016 Federal Election.

## Getting Started

These instructions will get you a copy of the forecast up and running on your local machine.

### Prerequisites

To install the dev environment locally, you'll need to install [conda](https://conda.io/projects/conda/en/latest/user-guide/install/)

### Installing

Install required packages using the buckleys_environment conda environment
```
conda env create -f environment_local.yml
```
Activate buckleys_public conda environment
```
source activate buckleys_environment
```
Run jupterlab
```
jupyter lab
```
Open the Run2016Forecast.ipynb

## Built With

* [PyMC3](https://docs.pymc.io/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [ThinkBayes](https://github.com/AllenDowney/ThinkBayes2)
